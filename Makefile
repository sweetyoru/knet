export OUTPUT_DIR :=$(shell pwd)/Output


.PHONY: all
all: release


.PHONY: debug
debug:
	mkdir -p $(OUTPUT_DIR)
	$(MAKE) -C Source debug
	$(MAKE) -C Document debug


.PHONY: release
release:
	mkdir -p $(OUTPUT_DIR)
	$(MAKE) -C Source release
	$(MAKE) -C Document release


.PHONY: run
run: all
	Output/BackEnd &
	Output/FrontEnd Output/frontEndConfig.json


.PHONY: clean
clean:
	$(MAKE) -C Source clean
	$(MAKE) -C Document clean
	rm -rf $(OUTPUT_DIR) 2>&1 >/dev/null
