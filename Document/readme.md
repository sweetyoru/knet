## 1. Overview

Clean, clear way to store note and all your knowledge.

Features:

- Native app, no electron bs that eat up your computer resource and still lag. GUI smoothness is the **top priority**.
- Written in rust.
- Work fully offline unless told otherwise.
- Self-hosted backend server. Your data is your.
- Free and open source, with MIT or GPL license depend on your taste.
- Support encrypted note, including peer-to-peer encryption and selective encryption.
- Support multiple backend server. In this case, the data will be duplicated on all listed backend servers, ensure data redundancy.
- Support multiuser.


## 2. Basic concepts

### 2.1. Node

Node is the building block in KNet. Each node has: unique id, content, tags and children. A node can has none or many child node. A node can also be child of many nodes. There is always a root node, which every node is a child of.


### 2.2. Node id

Node id is a unique number assigned to a node.


### 2.3. Node tag

Node tag is a simple string, useful for node category.


### 2.4. Node content

Node content can have: text, link to other nodes, link to internal attachments and link to external attachments. Internal attachment is like image inside a website, while enternal attachment is like attachement in email. Internal represent of node content is text.


### 2.5. Object

An object is anything that isn't a node. All attachments are saved as an object. Every object has a unique id.


### 2.6. Link

A link is used to link to a node from other nodes. A link is simply a string of target id.


### 2.7. User

A user is required to communicate backend. The backend require username and password for authentication.


### 2.8. Datastore

Data in backend is splited into datastores. Each datastore has an unique id and is independent of each others.


## 3. App model

The app include 2 parts:

- Frontend
- Backend


Frontend is the user interface of the app and doesn't store any data. Frontend will get data from backend to display to user. If encryption is turned on, then decryption key is derived from user supply password and kept inside the frontend, never go anywhere and deleted when the frontend exit.

Backend is where the data is saved.

Encryption and decryption are **always** done at frontend, backend **does not hold** any decryption key.


### 3.1. Frontend

The frontend is written in egui, using the eframe framework. Frontend has the ability to login into multiple backends and using multiple datastore at the same time. Each datastore in turn can be backed by multiple backends.


### 3.2. Backend

Backend can be anything that can communicate with frontend and save data. Data in backend is splited into multiple datastores, each datastore has an unique id and is independent of each others. Backend control which user has what access to which datastore.


### 3.3. User

A user is required to communicate with backend, each user is consist of username and password.


## 4. Quick start

### 4.1. Connect to backend

### 4.2. Create new datastore

### 4.3. Create new node

### 4.4. Add internal attachment

### 4.5. Add external attachment

### 4.6. Add tag

### 4.7. Link to other nodes


## 5. Trouble shooting

