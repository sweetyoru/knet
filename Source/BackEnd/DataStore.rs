use std::fmt;
use std::sync::{Arc, RwLock};
use std::collections::{BTreeSet, HashMap};

use hex::FromHexError;

use crate::User::{User, Permission};


#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ObjectID([u8; 64]);

impl ObjectID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Object
{
	objectID: ObjectID,
	isEncrypted: bool,
	objectData: Vec<u8>,
}

impl Object
{
	pub fn New (objectID: ObjectID, objectData: Vec<u8>) -> Self
	{
		Self
		{
			objectID,
			isEncrypted: false,
			objectData,
		}
	}

	pub fn ObjectID (&self) -> &ObjectID { &self.objectID }
	pub fn IsEncrypted (&self) -> bool { self.isEncrypted }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NodeID([u8; 64]);

impl NodeID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug, Clone)]
pub struct Node
{
	nodeID: NodeID,
	isEncrypted: bool,
	nodeData: Vec<u8>,
}


impl Node
{
	pub fn New (nodeID: NodeID) -> Self
	{
		Self
		{
			nodeID,
			isEncrypted: false,
			nodeData: Vec::new (),
		}
	}
}


#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DataStoreID([u8; 64]);

impl DataStoreID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug)]
pub struct DataStore
{
	id: DataStoreID,
	name: String,
	nodes: HashMap<NodeID, Node>,
	objects: HashMap<ObjectID, Object>,
}

impl DataStore
{
	pub fn New (id: &DataStoreID, name: &str) -> Self
	{
		Self
		{
			id: id.clone (),
			name: name.to_string (),
			nodes: HashMap::new (),
			objects: HashMap::new (),
		}
	}
}


static mut globalUsers: Option<RwLock<HashMap<String, Arc<RwLock<User>>>>> = None;
static mut globalDataStores: Option<RwLock<HashMap<DataStoreID, Arc<RwLock<DataStore>>>>> = None;


pub fn InitDataStore (configPath: &str) -> Result<(), DataStoreError>
{
	unsafe
	{
		globalUsers = Some(RwLock::new (HashMap::new ()));
		globalDataStores = Some(RwLock::new (HashMap::new ()));
	}

	LoadData (configPath)?;
	LoadPlaceHolderData ()
}


pub fn GetDataStore (dataStoreID: &DataStoreID) -> Option<Arc<RwLock<DataStore>>>
{
	unsafe
	{
		globalDataStores.as_ref ().unwrap ().read ().unwrap ().get (dataStoreID).map (|x| Arc::clone (x))
	}
}


fn LoadData (_configPath: &str) -> Result<(), DataStoreError>
{
	Ok(())
	/*
	let configFileContent = fs::read_to_string (configPath)?;
	let configData: Value = serde_json::from_str (&configFileContent)?;

	let mut dataStores = HashMap::new ();

	// get dataStores
	for jsonDataStore in configData["dataStores"].as_array ().unwrap ()
	{
		let id = DataStoreLocalID::deserialize (&jsonDataStore["id"])?;
		let name = String::deserialize (&jsonDataStore["name"])?;

		let mut backEndServers = Vec::new ();

		// get backEndServers
		for jsonBackEndServer in jsonDataStore["backEndServers"].as_array ().unwrap ()
		{
			//
			let address = IpAddr::deserialize (&jsonBackEndServer["address"])?;
			let port = u16::deserialize (&jsonBackEndServer["port"])?;
			let username = String::deserialize (&jsonBackEndServer["username"])?;
			let password = String::deserialize (&jsonBackEndServer["password"])?;

			let dataStoreID = DataStoreID::New (hex::decode (String::deserialize (&jsonBackEndServer["dataStoreID"])?)?.try_into ().unwrap ());
			let key = Aes256CipherKey::New (&hex::decode (String::deserialize (&jsonBackEndServer["key"])?)?.try_into ().unwrap ());
			let iv = hex::decode (String::deserialize (&jsonBackEndServer["iv"])?)?.try_into ().unwrap ();

			let readable = bool::deserialize (&jsonBackEndServer["readable"])?;
			let writable = bool::deserialize (&jsonBackEndServer["writable"])?;

			let user = User::New (&username, &password);

			let backEndServer = BackEndServer::New (address, port, user, dataStoreID, key, iv, readable, writable);

			backEndServers.push (backEndServer);
		}


		let dataStore = DataStore
		{
			id,
			name,
			backEndServers,
			nodeCache: HashMap::new (),
			objectCache: HashMap::new (),
		};

		dataStores.insert (id, dataStore);
	}

	unsafe
	{
		globalDataStores = Some(Mutex::new (dataStores));
	}

	Ok(())
	*/
}

fn LoadPlaceHolderData () -> Result<(), DataStoreError>
{
	/*
	unsafe
	{
		let dataStoresLock = globalDataStores.as_ref ().unwrap ();
		let mut dataStores = dataStoresLock.write ().unwrap ();

		// create some users
		let mut newUser1 = User::New ("user1", "pass1");
		let mut newUser2 = User::New ("user2", "pass2");
		let mut newUser3 = User::New ("user3", "pass3");
		let mut newUser4 = User::New ("user4", "pass4");
		let newUser5 = User::New ("user5", "pass5");


		// create some data id
		let dataStore1ID = DataStoreID::New ([1; 64]);
		let dataStore1Name = "data store 1";
		let mut dataStore1 = DataStore::New (&dataStore1ID, dataStore1Name);

		let dataStore2ID = DataStoreID::New ([2; 64]);
		let dataStore2Name = "data store 2";
		let mut dataStore2 = DataStore::New (&dataStore2ID, dataStore2Name);

		let dataStore3ID = DataStoreID::New ([3; 64]);
		let dataStore3Name = "data store 3";
		let mut dataStore3 = DataStore::New (&dataStore3ID, dataStore3Name);

		let dataStore4ID = DataStoreID::New ([4; 64]);
		let dataStore4Name = "data store 4";
		let mut dataStore4 = DataStore::New (&dataStore4ID, dataStore4Name);

		let dataStore5ID = DataStoreID::New ([5; 64]);
		let dataStore5Name = "data store 5";
		let mut dataStore5 = DataStore::New (&dataStore5ID, dataStore5Name);


		#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
		struct NodeTag(String);

		#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
		struct NodeContent
		{
			content: String,
		}

		impl NodeContent
		{
			pub fn New (content: &str) -> Self
			{
				Self
				{
					content: content.to_string (),
				}
			}
		}

		#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
		struct MyNode
		{
			nodeID: NodeID,
			isEncrypted: bool,
			nodeTitle: String,
			tags: BTreeSet<NodeTag>,
			childIDs: BTreeSet<NodeID>,
			content: NodeContent,
		}

		impl MyNode
		{
			pub fn ToNode (&self) -> Node
			{
				Node
				{
					nodeID: self.nodeID,
					isEncrypted: self.isEncrypted,
					nodeData: ser::to_vec_packed (self).unwrap (),
				}
			}
		}

		// make some node id
		let rootNodeID = NodeID::New ([0; 64]);
		let node1ID = NodeID::New ([1; 64]);
		let node2ID = NodeID::New ([2; 64]);
		let node3ID = NodeID::New ([3; 64]);
		let node4ID = NodeID::New ([4; 64]);
		let node5ID = NodeID::New ([5; 64]);


		// make more nodes
		let mut rootNode = MyNode
		{
			nodeID: rootNodeID,
			isEncrypted: false,
			nodeTitle: String::from ("root node"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		let mut node1 = MyNode
		{
			nodeID: node1ID,
			isEncrypted: false,
			nodeTitle: String::from ("node 1"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		let mut node2 = MyNode
		{
			nodeID: node2ID,
			isEncrypted: false,
			nodeTitle: String::from ("node 2"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		let mut node3 = MyNode
		{
			nodeID: node3ID,
			isEncrypted: false,
			nodeTitle: String::from ("node 3"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		let node4 = MyNode
		{
			nodeID: node4ID,
			isEncrypted: false,
			nodeTitle: String::from ("node 4"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		let node5 = MyNode
		{
			nodeID: node5ID,
			isEncrypted: false,
			nodeTitle: String::from ("node 5"),
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New ("node 1 content"),
		};

		// some tags
		rootNode.tags.insert (NodeTag (String::from ("root tag")));
		rootNode.tags.insert (NodeTag (String::from ("tag 1")));

		node1.tags.insert (NodeTag (String::from ("tag 1")));
		node1.tags.insert (NodeTag (String::from ("tag 2")));

		node2.tags.insert (NodeTag (String::from ("tag 2")));

		node3.tags.insert (NodeTag (String::from ("tag 1")));
		node3.tags.insert (NodeTag (String::from ("tag 2")));
		node3.tags.insert (NodeTag (String::from ("tag 3")));
		node3.tags.insert (NodeTag (String::from ("tag 4")));


		// childs
		rootNode.childIDs.insert (node1ID);
		rootNode.childIDs.insert (node2ID);

		node1.childIDs.insert (node3ID);
		node1.childIDs.insert (node4ID);

		node3.childIDs.insert (node5ID);


		// append node to data store
		dataStore1.nodes.insert (rootNodeID.clone (), rootNode.ToNode ());
		dataStore1.nodes.insert (node1ID.clone (), node1.ToNode ());
		dataStore1.nodes.insert (node2ID.clone (), node2.ToNode ());
		dataStore1.nodes.insert (node3ID.clone (), node3.ToNode ());
		dataStore1.nodes.insert (node4ID.clone (), node4.ToNode ());
		dataStore1.nodes.insert (node5ID.clone (), node5.ToNode ());

		dataStore2.nodes.insert (rootNodeID.clone (), rootNode.ToNode ());
		dataStore2.nodes.insert (node1ID.clone (), node1.ToNode ());
		dataStore2.nodes.insert (node2ID.clone (), node2.ToNode ());
		dataStore2.nodes.insert (node3ID.clone (), node3.ToNode ());
		dataStore2.nodes.insert (node4ID.clone (), node4.ToNode ());
		dataStore2.nodes.insert (node5ID.clone (), node5.ToNode ());

		dataStore3.nodes.insert (rootNodeID.clone (), rootNode.ToNode ());
		dataStore3.nodes.insert (node1ID.clone (), node1.ToNode ());
		dataStore3.nodes.insert (node2ID.clone (), node2.ToNode ());
		dataStore3.nodes.insert (node3ID.clone (), node3.ToNode ());
		dataStore3.nodes.insert (node4ID.clone (), node4.ToNode ());
		dataStore3.nodes.insert (node5ID.clone (), node5.ToNode ());

		dataStore4.nodes.insert (rootNodeID.clone (), rootNode.ToNode ());
		dataStore4.nodes.insert (node1ID.clone (), node1.ToNode ());
		dataStore4.nodes.insert (node2ID.clone (), node2.ToNode ());
		dataStore4.nodes.insert (node3ID.clone (), node3.ToNode ());
		dataStore4.nodes.insert (node4ID.clone (), node4.ToNode ());
		dataStore4.nodes.insert (node5ID.clone (), node5.ToNode ());

		dataStore5.nodes.insert (rootNodeID.clone (), rootNode.ToNode ());
		dataStore5.nodes.insert (node1ID.clone (), node1.ToNode ());
		dataStore5.nodes.insert (node2ID.clone (), node2.ToNode ());
		dataStore5.nodes.insert (node3ID.clone (), node3.ToNode ());
		dataStore5.nodes.insert (node4ID.clone (), node4.ToNode ());
		dataStore5.nodes.insert (node5ID.clone (), node5.ToNode ());


		// add user permissions
		newUser1.SetPermission (&dataStore1ID, Permission::ReadWrite);
		newUser1.SetPermission (&dataStore2ID, Permission::Read);
		newUser1.SetPermission (&dataStore3ID, Permission::Write);

		newUser2.SetPermission (&dataStore1ID, Permission::Read);
		newUser3.SetPermission (&dataStore1ID, Permission::Write);
		newUser4.SetPermission (&dataStore1ID, Permission::ReadWrite);

		// add user
		let usersLock = globalUsers.as_ref ().unwrap ();
		let mut users = usersLock.write ().unwrap ();

		users.insert (newUser1.Username ().to_string (), Arc::new (RwLock::new (newUser1)));
		users.insert (newUser2.Username ().to_string (), Arc::new (RwLock::new (newUser2)));
		users.insert (newUser3.Username ().to_string (), Arc::new (RwLock::new (newUser3)));
		users.insert (newUser4.Username ().to_string (), Arc::new (RwLock::new (newUser4)));
		users.insert (newUser5.Username ().to_string (), Arc::new (RwLock::new (newUser5)));

		// final add
		dataStores.insert (dataStore1ID, Arc::new (RwLock::new (dataStore1)));
		dataStores.insert (dataStore2ID, Arc::new (RwLock::new (dataStore2)));
		dataStores.insert (dataStore3ID, Arc::new (RwLock::new (dataStore3)));
		dataStores.insert (dataStore4ID, Arc::new (RwLock::new (dataStore4)));
		dataStores.insert (dataStore5ID, Arc::new (RwLock::new (dataStore5)));
	}
*/
	Ok(())
}



#[derive(Debug)]
pub enum DataStoreError
{
	NodeNotExist(NodeID),
	NodeExisted(NodeID),
	ObjectNotExist(ObjectID),
	ObjectExisted(ObjectID),
	CannotGetNode(NodeID),
	CannotGetObject(ObjectID),
	UnknownError,

	IOError(std::io::Error),
	JsonError(serde_json::Error),
	HexError(FromHexError),
	UninitializedConfig,
}


impl std::error::Error for DataStoreError {}

impl fmt::Display for DataStoreError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::NodeNotExist(nodeID) => format! ("Node not exist{:?}", nodeID),
			Self::NodeExisted(nodeID) => format! ("Node existed {:?}", nodeID),
			Self::ObjectNotExist(objectID) => format! ("Object not exist {:?}", objectID),
			Self::ObjectExisted(objectID) => format! ("Object existed {:?}", objectID),
			Self::CannotGetNode(nodeID) => format! ("Cannot get node {:?}", nodeID),
			Self::CannotGetObject(objectID) => format! ("Cannot get object {:?}", objectID),
			Self::UnknownError => format! ("Unknown error"),

			Self::IOError(error) => format! ("IO error: {}", error),
			Self::JsonError(error) => format! ("Json error: {}", error),
			Self::HexError(error) => format! ("Hex error: {}", error),
			Self::UninitializedConfig => String::from ("Uninitialized config"),
		};

		write! (f, "{}", result)
	}
}

impl From<std::io::Error> for DataStoreError
{
	fn from (error: std::io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<serde_json::Error> for DataStoreError
{
	fn from (error: serde_json::Error) -> Self
	{
		Self::JsonError(error)
	}
}

impl From<FromHexError> for DataStoreError
{
	fn from (error: FromHexError) -> Self
	{
		Self::HexError(error)
	}
}
