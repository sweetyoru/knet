use std::collections::HashMap;

use crate::DataStore::DataStoreID;


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Permission
{
	Read,
	Write,
	ReadWrite,
}


#[derive(Debug, Clone)]
pub struct User
{
	username: String,
	password: String,
	permissions: HashMap<DataStoreID, Permission>,
}

impl User
{
	pub fn New (username: &str, password: &str) -> Self
	{
		Self
		{
			username: username.to_string (),
			password: password.to_string (),
			permissions: HashMap::new (),
		}
	}

	pub fn Username (&self) -> &str { &self.username }
	pub fn Password (&self) -> &str { &self.password }
	pub fn GetPermission (&self, dataStoreID: &DataStoreID) -> Option<Permission>
	{
		self.permissions.get (dataStoreID).map (|x| *x)
	}

	pub fn RemovePermission (&mut self, dataStoreID: &DataStoreID)
	{
		self.permissions.remove (dataStoreID);
	}

	pub fn SetPermission (&mut self, dataStoreID: &DataStoreID, permission: Permission)
	{
		self.permissions.insert (dataStoreID.clone (), permission);
	}
}
