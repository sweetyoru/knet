#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(non_upper_case_globals)]	// turn off warning about global non upper camel name
#![allow(dead_code)]

use std::{io, fmt};
use std::net::{TcpListener, TcpStream};

use crate::DataStore::DataStoreError;


mod User;
mod DataStore;
mod FrontEnd;


fn main () -> Result<(), BackEndError>
{
	// bring a thread
	// do something like listen

	DataStore::InitDataStore ("")?;

	// listen loop
	loop
	{
		// wait for connection
		let listener = TcpListener::bind ("127.0.0.1:6666")?;

		// accept connections and process them serially
		for stream in listener.incoming ()
		{
			HandlerConnection (stream?);
		}
	}

	Ok(())
}


fn HandlerConnection (tcpStream: TcpStream)
{
	todo! ();
}






#[derive (Debug)]
pub enum BackEndError
{
	IOError(io::Error),
	NoConfigPath,
	DataStoreError(DataStoreError),
	ParseIntError(std::num::ParseIntError),
	UnimplementedError,
}


impl std::error::Error for BackEndError {}

impl fmt::Display for BackEndError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::IOError(ioError) => String::from (format! ("IO error: {}", ioError)),
			Self::NoConfigPath => String::from ("No config path"),
			Self::DataStoreError(dataStoreError) => String::from (format! ("Datastore error: {}", dataStoreError)),
			Self::ParseIntError(error) => String::from (format! ("Parse integer error: {}", error)),
			Self::UnimplementedError => String::from ("This error is not implemented"),
		};

		write! (f, "{}", result)
	}
}

impl From<io::Error> for BackEndError
{
	fn from (error: io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<DataStoreError> for BackEndError
{
	fn from (error: DataStoreError) -> Self
	{
		Self::DataStoreError(error)
	}
}

impl From<std::num::ParseIntError> for BackEndError
{
	fn from (error: std::num::ParseIntError) -> Self
	{
		Self::ParseIntError(error)
	}
}
