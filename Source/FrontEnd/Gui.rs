use std::sync::Arc;
use std::{fs, fmt, mem};

use eframe::egui::containers::CentralPanel;
use eframe::{Frame, App, CreationContext, NativeOptions, run_native};
use eframe::egui::
	{
		self,
		Context, Ui,
		TopBottomPanel, SidePanel,
		CollapsingHeader,
		TextEdit, ScrollArea,
	};

use serde_json::Value;
use serde::Deserialize;

use crate::GlobalData;
use crate::DataStore::{self, NodeID, DataStoreLocalID};


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Deserialize)]
enum Size
{
	Pixel(usize),
}


#[derive(Debug, Clone)]
struct GuiConfig
{
	menuBarSize: Size,
	statusBarSize: Size,
	nodeListPanelSize: Size,
	allowNodeListPanelScrollHorizontally: bool,
	allowNodeNameWrapping: bool,
	menuBarHeight: Size,
	statusBarHeight: Size,
}

impl GuiConfig
{
	pub fn MenuBarSize (&self) -> Size { self.menuBarSize }
	pub fn StatusBarSize (&self) -> Size { self.statusBarSize }
	pub fn NodeListPanelSize (&self) -> Size { self.nodeListPanelSize }
	pub fn AllowNodeListPanelScrollHorizontally (&self) -> bool { self.allowNodeListPanelScrollHorizontally }
	pub fn AllowNodeNameWrapping (&self) -> bool { self.allowNodeNameWrapping }
	pub fn MenuBarHeight (&self) -> Size { self.menuBarHeight }
	pub fn StatusBarHeight (&self) -> Size { self.statusBarHeight }
}


#[derive(Debug)]
pub struct AppGui;

impl AppGui
{
	pub fn New (_creationContext: &CreationContext) -> Self { Self }


	fn DrawMenuBar (&mut self, ui: &mut Ui)
	{
		ui.label ("Menu bar");
	}

	fn DrawStatusBar (&mut self, ui: &mut Ui)
	{
		ui.label ("Status bar");
	}

	fn DrawToolPanel (&mut self, ui: &mut Ui)
	{
		ui.label ("Tool panel");
	}

	fn DrawNodeListPanel (&mut self, ui: &mut Ui)
	{
		fn DrawNode (ui: &mut Ui, dataStoreLocalID: DataStoreLocalID, nodeID: &NodeID)
		{
			fn circle_icon (ui: &mut egui::Ui, openness: f32, response: &egui::Response)
			{
				let stroke = ui.style ().interact (&response).fg_stroke;
				let radius = egui::lerp (2.0..=6.0, openness);
				ui.painter ().circle_filled (response.rect.center (), radius, stroke.color);
			}

			let dataStoreLock = DataStore::GetDataStore (&dataStoreLocalID).unwrap ();
			let mut dataStore = dataStoreLock.write ().unwrap ();
			let currentNode = dataStore.GetNode (nodeID).unwrap ();


			// get the node title
			let nodeTitle = currentNode.NodeTitle ().clone ();
			let childIDs = currentNode.ChildIDs ().clone ();


			// drop the locks
			mem::drop (currentNode);
			mem::drop (dataStore);


			// draw current node
			CollapsingHeader::new (nodeTitle)
				.icon (circle_icon)
				.show (ui, |ui|
				{
					for childID in childIDs
					{
						DrawNode (ui, dataStoreLocalID, &childID);
					}
				});
		}

		let guiConfig = GetGuiConfig ().unwrap ();
		let globalData = GlobalData::GetGlobalData ().read ().unwrap ();
		let currentDataStoreLocalID = globalData.CurrentDataStoreLocalID ().unwrap ();

		ScrollArea::both ()
			.hscroll (guiConfig.AllowNodeListPanelScrollHorizontally ())
			.auto_shrink ([false; 2])
			.show (ui, |ui| DrawNode (ui, currentDataStoreLocalID, &DataStore::GetRootNodeID ()));
	}

	fn DrawNoteTabPanel (&mut self, ui: &mut Ui)
	{
		let globalData = GlobalData::GetGlobalData ().read ().unwrap ();
		let currentDataStoreLocalID = globalData.CurrentDataStoreLocalID ().unwrap ();
		let dataStoreLock = DataStore::GetDataStore (&currentDataStoreLocalID).unwrap ();
		let mut dataStore = dataStoreLock.write ().unwrap ();


		ui.horizontal (|ui|
			{
				let globalData = GlobalData::GetGlobalData ().read ().unwrap ();

				for openNodeID in globalData.OpenNodeIDs ()
				{
					let currentNode = dataStore.GetNode (openNodeID).unwrap ();

					// get the node title
					let nodeTitle = currentNode.NodeTitle ();

					if *globalData.CurrentNodeID ().unwrap () == *openNodeID
					{
						ui.strong (nodeTitle);
					}
					else
					{
						ui.label (nodeTitle);
					}
				}
			});
	}

	fn DrawEditor (&mut self, ui: &mut Ui)
	{
		let globalData = GlobalData::GetGlobalData ().write ().unwrap ();

		if let Some(openNodeID) = globalData.CurrentNodeID ()
		{
			let globalData = GlobalData::GetGlobalData ().read ().unwrap ();
			let currentDataStoreLocalID = globalData.CurrentDataStoreLocalID ().unwrap ();
			let dataStoreLock = DataStore::GetDataStore (&currentDataStoreLocalID).unwrap ();
			let mut dataStore = dataStoreLock.write ().unwrap ();
			let currentNode = dataStore.GetNode (openNodeID).unwrap ();

			TextEdit::multiline (currentNode)
				.hint_text ("Type something!")
				.show (ui);

			// after this, the data maybe updated
			// tell data store to check for update
			dataStore.UpdateNode (&openNodeID).unwrap ();
		}
	}

	fn DrawScreen (&mut self, ui: &mut Ui)
	{
		TopBottomPanel::top ("MenuBar").show (ui.ctx (), |ui| self.DrawMenuBar (ui));
		TopBottomPanel::bottom ("StatusBar").show (ui.ctx (), |ui| self.DrawStatusBar (ui));
		SidePanel::left ("NodeListPanel").show (ui.ctx (), |ui| self.DrawNodeListPanel (ui));

		TopBottomPanel::top ("NodeTabPanel").show (ui.ctx (), |ui| self.DrawNoteTabPanel (ui));
		TopBottomPanel::top ("ToolPanel").show (ui.ctx (), |ui| self.DrawToolPanel (ui));

		CentralPanel::default().show (ui.ctx (), |ui| self.DrawEditor (ui));
	}
}


impl App for AppGui
{
	fn update(&mut self, context: &Context, _frame: &mut Frame)
	{
		CentralPanel::default ().show (context, |ui| self.DrawScreen (ui));
	}
}


static mut globalGuiConfig: Option<Arc<GuiConfig>> = None;


pub fn InitGuiConfig (guiConfigPath: &str) -> Result<(), GuiConfigError>
{
	let guiConfigFileContent = fs::read_to_string (guiConfigPath)?;
	let guiConfigData: Value = serde_json::from_str (&guiConfigFileContent)?;

	unsafe
	{
		globalGuiConfig = Some(Arc::new (GuiConfig
		{
			menuBarSize: Size::deserialize (&guiConfigData["MenuBarSize"])?,
			statusBarSize: Size::deserialize (&guiConfigData["StatusBarSize"])?,
			nodeListPanelSize: Size::deserialize (&guiConfigData["NodeListPanelSize"])?,
			allowNodeListPanelScrollHorizontally: bool::deserialize (&guiConfigData["AllowNodeListPanelScrollHorizontally"])?,
			allowNodeNameWrapping: bool::deserialize (&guiConfigData["AllowNodeNameWrapping"])?,
			menuBarHeight: Size::deserialize (&guiConfigData["MenuBarHeight"])?,
			statusBarHeight: Size::deserialize (&guiConfigData["StatusBarHeight"])?,
		}));
	}

	Ok(())
}


fn GetGuiConfig () -> Result<Arc<GuiConfig>, GuiConfigError>
{
	unsafe
	{
		match &globalGuiConfig
		{
			Some(guiConfig) => Ok(Arc::clone (guiConfig)),
			None => Err(GuiConfigError::UninitializedConfig),
		}
	}
}


pub fn StartGui ()
{
	let native_options = NativeOptions::default ();
	run_native ("KNet frontend", native_options, Box::new (|creationContext| Box::new (AppGui::New (creationContext))));
}


#[derive (Debug)]
pub enum GuiConfigError
{
	IOError(std::io::Error),
	JsonError(serde_json::Error),
	UninitializedConfig,
}

impl std::error::Error for GuiConfigError {}

impl fmt::Display for GuiConfigError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::IOError(error) => format! ("IO error: {}", error),
			Self::JsonError(error) => format! ("Json error: {}", error),
			Self::UninitializedConfig => String::from ("Uninitialized config"),
		};

		write! (f, "{}", result)
	}
}

impl From<std::io::Error> for GuiConfigError
{
	fn from (error: std::io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<serde_json::Error> for GuiConfigError
{
	fn from (error: serde_json::Error) -> Self
	{
		Self::JsonError(error)
	}
}
