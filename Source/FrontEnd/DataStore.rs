use core::ops::Range;

use std::net::IpAddr;
use std::{fs, fmt};
use std::sync::{Arc, RwLock};
use std::collections::{BTreeSet, HashMap};

use hex::FromHexError;
use serde_json::Value;
use KotoDerive::KotoTrait;
use eframe::egui::TextBuffer;
use EasyCrypto::Aes256CipherKey;
use Koto::{KotoTrait, KotoError};
use serde::{Serialize, Deserialize};

use crate::BackEnd::{BackEndServer, BackEndError, DataStoreID, User};


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct DataStoreLocalID(usize);

impl DataStoreLocalID
{
	pub fn New (id: usize) -> Self { Self(id) }
}


#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, KotoTrait)]
pub struct ObjectID([u8; 64]);

impl ObjectID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash, KotoTrait)]
pub struct Object
{
	objectID: ObjectID,
	isEncrypted: bool,
	objectData: Vec<u8>,
}

impl Object
{
	pub fn New (objectID: ObjectID, objectData: Vec<u8>) -> Self
	{
		Self
		{
			objectID,
			isEncrypted: false,
			objectData,
		}
	}

	pub fn ObjectID (&self) -> &ObjectID { &self.objectID }
	pub fn IsEncrypted (&self) -> bool { self.isEncrypted }
}


#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, KotoTrait)]
pub struct NodeID([u8; 64]);

impl NodeID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, KotoTrait)]
pub struct NodeTag(String);


#[derive(Debug, Clone, PartialEq, Eq, Hash, KotoTrait)]
pub struct NodeContent
{
	content: String,
}

impl NodeContent
{
	pub fn New (content: String) -> Self
	{
		Self
		{
			content,
		}
	}

	pub fn InsertText (&mut self, text: &str, charIndex: usize) -> usize
	{
		// Get the byte index from the character index
		let byteIndex = self.content.byte_index_from_char_index (charIndex);

		// Then insert the string
		self.content.insert_str (byteIndex, text);

		text.chars ().count ()
	}

	pub fn DeleteCharRange (&mut self, charRange: Range<usize>)
	{
		assert! (charRange.start <= charRange.end);

		// Get both byte indices
		let byteIndexStart = self.content.byte_index_from_char_index (charRange.start);
		let byteIndexEnd = self.content.byte_index_from_char_index (charRange.end);

		// Then drain all characters within this range
		self.content.drain (byteIndexStart..byteIndexEnd);
	}
}


impl AsRef<str> for NodeContent
{
	fn as_ref (&self) -> &str { &self.content }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash, KotoTrait)]
pub struct Node
{
	isDirty: bool,
	nodeID: NodeID,
	isEncrypted: bool,
	nodeTitle: String,
	tags: BTreeSet<NodeTag>,
	childIDs: BTreeSet<NodeID>,
	content: NodeContent,
}


impl Node
{
	pub fn New (nodeID: NodeID, nodeTitle: String) -> Self
	{
		Self
		{
			isDirty: false,
			nodeID,
			isEncrypted: false,
			nodeTitle,
			tags: BTreeSet::new (),
			childIDs: BTreeSet::new (),
			content: NodeContent::New (String::new ()),
		}
	}

	pub fn IsDirty (&self) -> bool { self.isDirty }
	pub fn NodeID (&self) -> &NodeID { &self.nodeID }
	pub fn IsEncrypted (&self) -> bool { self.isEncrypted }
	pub fn NodeTitle (&self) -> &String { &self.nodeTitle }
	pub fn Tags (&self) -> &BTreeSet<NodeTag> { &self.tags }
	pub fn ChildIDs (&self) -> &BTreeSet<NodeID> { &self.childIDs }
	pub fn Content (&self) -> &NodeContent { &self.content }

	pub fn SetDirty (&mut self) { self.isDirty = true; }
	pub fn SetClean (&mut self) { self.isDirty = false; }

	pub fn ChangeNodeTitle (&mut self, newNodeTitle: String)
	{
		self.nodeTitle = newNodeTitle;
	}

	pub fn AddTag (&mut self, tag: NodeTag) -> Result<(), NodeError>
	{
		if self.tags.contains (&tag)
		{
			Err(NodeError::DuplicatedTag(tag))
		}
		else
		{
			self.tags.insert (tag);
			Ok(())
		}
	}

	pub fn RemoveTag (&mut self, tag: &NodeTag) -> Result<(), NodeError>
	{
		if self.tags.remove (tag)
		{
			Err(NodeError::TagNotExist(tag.clone ()))
		}
		else
		{
			Ok(())
		}
	}

	pub fn AddChild (&mut self, childID: NodeID) -> Result<(), NodeError>
	{
		if self.childIDs.contains (&childID)
		{
			Err(NodeError::DuplicatedChild(childID))
		}
		else
		{
			self.childIDs.insert (childID);
			Ok(())
		}
	}

	pub fn RemoveChild (&mut self, childID: &NodeID) -> Result<(), NodeError>
	{
		if self.childIDs.remove (childID)
		{
			Err(NodeError::ChildNotExist(childID.clone ()))
		}
		else
		{
			Ok(())
		}
	}
}


impl AsRef<str> for Node
{
	fn as_ref (&self) -> &str { self.content.as_ref () }
}

impl TextBuffer for Node
{
	fn is_mutable (&self) -> bool { true }

	fn insert_text (&mut self, text: &str, charIndex: usize) -> usize
	{
		self.SetDirty ();
		self.content.InsertText (text, charIndex)
	}

	fn delete_char_range (&mut self, charRange: Range<usize>)
	{
		self.SetDirty ();
		self.content.DeleteCharRange (charRange);
	}
}


#[derive(Debug)]
pub enum NodeError
{
	TagNotExist(NodeTag),
	DuplicatedTag(NodeTag),
	ChildNotExist(NodeID),
	DuplicatedChild(NodeID),
}


#[derive(Debug)]
pub struct DataStore
{
	id: DataStoreLocalID,
	name: String,
	backEndServers: Vec<BackEndServer>,
	nodeCache: HashMap<NodeID, Node>,
	objectCache: HashMap<ObjectID, Object>,
}


impl<'a> DataStore
{
	pub fn New (id: DataStoreLocalID, name: String) -> Self
	{
		Self
		{
			id,
			name,
			backEndServers: Vec::new (),
			nodeCache: HashMap::new (),
			objectCache: HashMap::new (),
		}
	}

	pub fn ID (&self) -> DataStoreLocalID { self.id }
	pub fn Name (&self) -> &str { &self.name }

	/// This function will create a new node and sync change with backend
	pub fn CreateNode (&mut self, newNode: Node) -> Result<(), Vec<DataStoreError>>
	{
		let nodeID = newNode.NodeID ().clone ();

		if self.nodeCache.contains_key (&nodeID)
		{
			Err(vec! [DataStoreError::NodeExisted(nodeID)])
		}
		else
		{
			let result = self.CreateNodeFromBackEnd (&newNode);
			self.nodeCache.insert (nodeID, newNode);
			result
		}
	}

	/// This function will attempt get node from cache first
	/// If failed, query backend and update cache
	pub fn GetNode (&'a mut self, nodeID: &NodeID) -> Result<&'a mut Node, Vec<DataStoreError>>
	{
		if self.nodeCache.contains_key (nodeID)
		{
			Ok(self.nodeCache.get_mut (nodeID).unwrap ())
		}
		else
		{
			// node is not in cache, query backend
			self.GetNodeFromBackEnd (nodeID)?;

			// this should success
			Ok(self.nodeCache.get_mut (nodeID).unwrap ())
		}
	}

	/// This function will sync data back to backend
	pub fn UpdateNode (&mut self, nodeID: &NodeID) -> Result<(), Vec<DataStoreError>>
	{
		if self.nodeCache.get (nodeID).unwrap ().IsDirty ()
		{
			self.UpdateNodeFromBackEnd (nodeID)
		}
		else
		{
			Ok(())
		}
	}

	/// This function will delete the node from both cache and backend
	pub fn DeleteNode (&mut self, nodeID: &NodeID) -> Result<(), Vec<DataStoreError>>
	{
		if self.nodeCache.remove (nodeID).is_none ()
		{
			Err(vec! [DataStoreError::NodeNotExist(nodeID.clone ())])
		}
		else
		{
			self.DeleteNodeFromBackEnd (nodeID)
		}
	}


	fn CreateNodeFromBackEnd (&mut self, newNode: &Node) -> Result<(), Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();

		for backEndServer in &mut self.backEndServers
		{
			if backEndServer.Writable ()
			{
				if let Err(backEndError) = backEndServer.CreateNode (newNode)
				{
					dataStoreErrors.push (DataStoreError::BackEndError(backEndError));
				}
			}
		}

		if dataStoreErrors.len () == 0
		{
			Ok(())
		}
		else
		{
			Err(dataStoreErrors)
		}
	}

	fn GetNodeFromBackEnd (&mut self, nodeID: &NodeID) -> Result<Node, Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();
		let mut nodes = Vec::new ();

		// 5 cases:
		// - nodes empty, can't connect to any backend -> return backend errors
		// - nodes same, no error
		// - nodes same, backend errors
		// - nodes different, no error
		// - nodes different, backend error

		for backEndServer in &mut self.backEndServers
		{
			match backEndServer.GetNode (nodeID)
			{
				Ok(node) => nodes.push (node),
				Err(backEndError) => dataStoreErrors.push (DataStoreError::BackEndError(backEndError)),
			}
		}

		if nodes.len () == 0
		{
			dataStoreErrors.push (DataStoreError::CannotGetNode (nodeID.clone ()));
			Err(dataStoreErrors)
		}
		else
		{
			let firstNode = nodes.pop ().unwrap ();

			if nodes.iter ().all (|n| *n == firstNode)
			{
				dataStoreErrors.push (DataStoreError::NodeConflict (nodes));
			}

			if dataStoreErrors.len () > 0
			{
				Err(dataStoreErrors)
			}
			else
			{
				Ok(firstNode)
			}
		}
	}

	fn UpdateNodeFromBackEnd (&mut self, nodeID: &NodeID) -> Result<(), Vec<DataStoreError>>
	{
		let node = self.nodeCache.get (nodeID).unwrap ();
		let mut dataStoreErrors = Vec::new ();

		for backEndServer in &mut self.backEndServers
		{
			if backEndServer.Writable ()
			{
				if let Err(backEndError) = backEndServer.UpdateNode (node)
				{
					dataStoreErrors.push (DataStoreError::BackEndError(backEndError));
				}
			}
		}

		if dataStoreErrors.len () == 0
		{
			Ok(())
		}
		else
		{
			Err(dataStoreErrors)
		}
	}

	fn DeleteNodeFromBackEnd (&mut self, nodeID: &NodeID) -> Result<(), Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();

		for backEndServer in &mut self.backEndServers
		{
			if backEndServer.Writable ()
			{
				if let Err(backEndError) = backEndServer.DeleteNode (nodeID)
				{
					dataStoreErrors.push (DataStoreError::BackEndError(backEndError));
				}
			}
		}

		if dataStoreErrors.len () == 0
		{
			Ok(())
		}
		else
		{
			Err(dataStoreErrors)
		}
	}


	/// This function will create a new object and sync change with backend
	pub fn CreateObject (&mut self, newObject: Object) -> Result<(), Vec<DataStoreError>>
	{
		let objectID = newObject.ObjectID ().clone ();

		if self.objectCache.contains_key (&objectID)
		{
			Err(vec! [DataStoreError::ObjectExisted(objectID)])
		}
		else
		{
			let result = self.CreateObjectFromBackEnd (&newObject);
			self.objectCache.insert (objectID, newObject);
			result
		}
	}

	/// This function will attempt get object from cache first
	/// If failed, query backend and update cache
	pub fn GetObject (&mut self, objectID: &ObjectID) -> Result<&mut Object, Vec<DataStoreError>>
	{
		if self.objectCache.contains_key (objectID)
		{
			Ok(self.objectCache.get_mut (objectID).unwrap ())
		}
		else
		{
			// node is not in cache, query backend
			self.GetObjectFromBackEnd (objectID)?;

			// this should success
			Ok(self.objectCache.get_mut (objectID).unwrap ())
		}
	}

	/// This function will delete the object from both cache and backend
	pub fn DeleteObject (&mut self, objectID: &ObjectID) -> Result<(), Vec<DataStoreError>>
	{
		if self.objectCache.remove (objectID).is_none ()
		{
			Err(vec! [DataStoreError::ObjectNotExist(objectID.clone ())])
		}
		else
		{
			self.DeleteObjectFromBackEnd (objectID)
		}
	}


	fn CreateObjectFromBackEnd (&mut self, newObject: &Object) -> Result<(), Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();

		for backEndServer in &mut self.backEndServers
		{
			if backEndServer.Writable ()
			{
				if let Err(backEndError) = backEndServer.CreateObject (newObject)
				{
					dataStoreErrors.push (DataStoreError::BackEndError(backEndError));
				}
			}
		}

		if dataStoreErrors.len () == 0
		{
			Ok(())
		}
		else
		{
			Err(dataStoreErrors)
		}
	}

	fn GetObjectFromBackEnd (&mut self, objectID: &ObjectID) -> Result<Object, Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();
		let mut objects = Vec::new ();

		// 5 cases:
		// - objects empty, can't connect to any backend -> return backend errors
		// - objects same, no error
		// - objects same, backend errors
		// - objects different, no error
		// - objects different, backend error


		for backEndServer in &mut self.backEndServers
		{
			match backEndServer.GetObject (objectID)
			{
				Ok(object) => objects.push (object),
				Err(backEndError) => dataStoreErrors.push (DataStoreError::BackEndError(backEndError)),
			}
		}

		if objects.len () == 0
		{
			dataStoreErrors.push (DataStoreError::CannotGetObject (objectID.clone ()));
		}

		let firstObject = objects.pop ().unwrap ();

		if objects.iter ().all (|n| *n == firstObject)
		{
			dataStoreErrors.push (DataStoreError::ObjectConflict (objects));
		}

		if dataStoreErrors.len () > 0
		{
			Err(dataStoreErrors)
		}
		else
		{
			Ok(firstObject)
		}
	}

	fn DeleteObjectFromBackEnd (&mut self, objectID: &ObjectID) -> Result<(), Vec<DataStoreError>>
	{
		let mut dataStoreErrors = Vec::new ();

		for backEndServer in &mut self.backEndServers
		{
			if backEndServer.Writable ()
			{
				if let Err(backEndError) = backEndServer.DeleteObject (objectID)
				{
					dataStoreErrors.push (DataStoreError::BackEndError(backEndError));
				}
			}
		}

		if dataStoreErrors.len () == 0
		{
			Ok(())
		}
		else
		{
			Err(dataStoreErrors)
		}
	}
}


static mut globalDataStores: Option<RwLock<HashMap<DataStoreLocalID, Arc<RwLock<DataStore>>>>> = None;


pub fn InitDataStore (configPath: &str) -> Result<(), DataStoreError>
{
	let configFileContent = fs::read_to_string (configPath)?;
	let configData: Value = serde_json::from_str (&configFileContent)?;

	let mut dataStores = HashMap::new ();

	// get dataStores
	for jsonDataStore in configData["dataStores"].as_array ().unwrap ()
	{
		let id = DataStoreLocalID::deserialize (&jsonDataStore["id"])?;
		let name = String::deserialize (&jsonDataStore["name"])?;

		let mut backEndServers = Vec::new ();

		// get backEndServers
		for jsonBackEndServer in jsonDataStore["backEndServers"].as_array ().unwrap ()
		{
			//
			let address = IpAddr::deserialize (&jsonBackEndServer["address"])?;
			let port = u16::deserialize (&jsonBackEndServer["port"])?;
			let username = String::deserialize (&jsonBackEndServer["username"])?;
			let password = String::deserialize (&jsonBackEndServer["password"])?;

			let dataStoreID = DataStoreID::New (hex::decode (String::deserialize (&jsonBackEndServer["dataStoreID"])?)?.try_into ().unwrap ());
			let key = Aes256CipherKey::New (&hex::decode (String::deserialize (&jsonBackEndServer["key"])?)?.try_into ().unwrap ());
			let iv = hex::decode (String::deserialize (&jsonBackEndServer["iv"])?)?.try_into ().unwrap ();

			let readable = bool::deserialize (&jsonBackEndServer["readable"])?;
			let writable = bool::deserialize (&jsonBackEndServer["writable"])?;

			let user = User::New (&username, &password);

			let backEndServer = BackEndServer::New (address, port, user, dataStoreID, key, iv, readable, writable);

			backEndServers.push (backEndServer);
		}


		let dataStore = DataStore
		{
			id,
			name,
			backEndServers,
			nodeCache: HashMap::new (),
			objectCache: HashMap::new (),
		};

		dataStores.insert (id, Arc::new (RwLock::new (dataStore)));
	}

	unsafe
	{
		globalDataStores = Some(RwLock::new (dataStores));
	}

	Ok(())
}


pub fn GetDataStore (dataStoreLocalID: &DataStoreLocalID) -> Option<Arc<RwLock<DataStore>>>
{
	unsafe
	{
		globalDataStores.as_ref ().unwrap ().read ().unwrap ().get (dataStoreLocalID).map (|x| Arc::clone (x))
	}
}


pub fn GetRootNodeID () -> NodeID { NodeID([0; 64]) }


#[derive(Debug)]
pub enum DataStoreError
{
	NodeNotExist(NodeID),
	NodeExisted(NodeID),
	ObjectNotExist(ObjectID),
	ObjectExisted(ObjectID),
	BackEndError(BackEndError),
	CannotGetNode(NodeID),
	NodeConflict(Vec<Node>),
	CannotGetObject(ObjectID),
	ObjectConflict(Vec<Object>),
	UnknownError,

	IOError(std::io::Error),
	JsonError(serde_json::Error),
	HexError(FromHexError),
	UninitializedConfig,
}


impl std::error::Error for DataStoreError {}

impl fmt::Display for DataStoreError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::NodeNotExist(nodeID) => format! ("Node not exist{:?}", nodeID),
			Self::NodeExisted(nodeID) => format! ("Node existed {:?}", nodeID),
			Self::ObjectNotExist(objectID) => format! ("Object not exist {:?}", objectID),
			Self::ObjectExisted(objectID) => format! ("Object existed {:?}", objectID),
			Self::BackEndError(backEndError) => format! ("Backend error {:?}", backEndError),
			Self::CannotGetNode(nodeID) => format! ("Cannot get node {:?}", nodeID),
			Self::NodeConflict(nodes) => format! ("Node conflict {:?}", nodes),
			Self::CannotGetObject(objectID) => format! ("Cannot get object {:?}", objectID),
			Self::ObjectConflict(objects) => format! ("Object conflict {:?}", objects),
			Self::UnknownError => format! ("Unknown error"),

			Self::IOError(error) => format! ("IO error: {}", error),
			Self::JsonError(error) => format! ("Json error: {}", error),
			Self::HexError(error) => format! ("Hex error: {}", error),
			Self::UninitializedConfig => String::from ("Uninitialized config"),
		};

		write! (f, "{}", result)
	}
}

impl From<std::io::Error> for DataStoreError
{
	fn from (error: std::io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<serde_json::Error> for DataStoreError
{
	fn from (error: serde_json::Error) -> Self
	{
		Self::JsonError(error)
	}
}

impl From<FromHexError> for DataStoreError
{
	fn from (error: FromHexError) -> Self
	{
		Self::HexError(error)
	}
}
