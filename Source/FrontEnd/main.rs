#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(non_upper_case_globals)]	// turn off warning about global non upper camel name
#![allow(dead_code)]
#![feature(const_btree_new)]


mod Config;
mod DataStore;
mod BackEnd;
mod GlobalData;
mod Gui;
mod Misc;


use std::{io, env, fmt};

use crate::Config::ConfigError;
use crate::Gui::GuiConfigError;
use crate::DataStore::DataStoreError;


fn main () -> Result<(), FrontEndError>
{
	// init config
	if env::args ().len () != 2
	{
		println! ("Usage: ./FrontEnd [config path]");
		return Err(FrontEndError::NoConfigPath)
	}

	let configPath = env::args ().nth (1).unwrap ();

	Config::InitGlobalConfig (&configPath)?;


	// init global data
	GlobalData::InitGlobalData ();


	// init data store
	DataStore::InitDataStore (&configPath)?;


	// init gui config
	Gui::InitGuiConfig (&Config::GetGlobalAppConfig ()?.read ().unwrap ().GuiConfigPath ())?;

	LoadDefaultData ();


	// start the gui
	Gui::StartGui ();


	Ok(())

}


fn LoadDefaultData ()
{
	use crate::DataStore::DataStoreLocalID;
	GlobalData::GetGlobalData ().write ().unwrap ().SetCurrentDataStoreLocalID (Some(DataStoreLocalID::New (1)));
}


#[derive (Debug)]
pub enum FrontEndError
{
	IOError(io::Error),
	NoConfigPath,
	ConfigError(ConfigError),
	DataStoreError(DataStoreError),
	GuiConfigError(GuiConfigError),
	ParseIntError(std::num::ParseIntError),
	UnimplementedError,
}


impl std::error::Error for FrontEndError {}

impl fmt::Display for FrontEndError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::IOError(ioError) => String::from (format! ("IO error: {}", ioError)),
			Self::NoConfigPath => String::from ("No config path"),
			Self::ConfigError(configError) => String::from (format! ("Config error: {}", configError)),
			Self::DataStoreError(dataStoreError) => String::from (format! ("Datastore error: {}", dataStoreError)),
			Self::GuiConfigError(guiConfigError) => String::from (format! ("GUI config error: {}", guiConfigError)),
			Self::ParseIntError(error) => String::from (format! ("Parse integer error: {}", error)),
			Self::UnimplementedError => String::from ("This error is not implemented"),
		};

		write! (f, "{}", result)
	}
}

impl From<io::Error> for FrontEndError
{
	fn from (error: io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<ConfigError> for FrontEndError
{
	fn from (error: ConfigError) -> Self
	{
		Self::ConfigError(error)
	}
}

impl From<DataStoreError> for FrontEndError
{
	fn from (error: DataStoreError) -> Self
	{
		Self::DataStoreError(error)
	}
}

impl From<GuiConfigError> for FrontEndError
{
	fn from (error: GuiConfigError) -> Self
	{
		Self::GuiConfigError(error)
	}
}

impl From<std::num::ParseIntError> for FrontEndError
{
	fn from (error: std::num::ParseIntError) -> Self
	{
		Self::ParseIntError(error)
	}
}
