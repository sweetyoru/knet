use std::sync::RwLock;
use std::collections::BTreeSet;

use crate::DataStore::{NodeID, DataStoreLocalID};


#[derive(Debug, Clone)]
pub struct GlobalData
{
	currentDataStoreLocalID: Option<DataStoreLocalID>,
	currentNodeID: Option<NodeID>,
	openNodeIDs: BTreeSet<NodeID>,
}

impl GlobalData
{
	pub const fn New () -> Self
	{
		Self
		{
			currentDataStoreLocalID: None,
			currentNodeID: None,
			openNodeIDs: BTreeSet::new (),
		}
	}

	pub fn CurrentDataStoreLocalID (&self) -> Option<DataStoreLocalID>
	{
		self.currentDataStoreLocalID
	}

	pub fn SetCurrentDataStoreLocalID (&mut self, dataStoreLocalID: Option<DataStoreLocalID>)
	{
		self.currentDataStoreLocalID = dataStoreLocalID;
	}

	pub fn CurrentNodeID (&self) -> Option<&NodeID>
	{
		self.currentNodeID.as_ref ()
	}

	pub fn SetCurrentNodeID (&mut self, currentNodeID: Option<NodeID>)
	{
		self.currentNodeID = currentNodeID;
	}

	pub fn OpenNodeIDs (&self) -> &BTreeSet<NodeID> { &self.openNodeIDs }

	pub fn AddOpenNodeID (&mut self, openNodeID: &NodeID)
	{
		self.openNodeIDs.insert (openNodeID.clone ());
	}

	pub fn RemoveOpenNodeID (&mut self, openNodeID: &NodeID)
	{
		self.openNodeIDs.remove (openNodeID);
	}
}


static mut globalData: Option<RwLock<GlobalData>> = None;


pub fn InitGlobalData ()
{
	unsafe
	{
		globalData = Some(RwLock::new (GlobalData::New ()));
	}
}

pub fn GetGlobalData () -> &'static RwLock<GlobalData>
{
	unsafe { globalData.as_ref ().unwrap () }
}
