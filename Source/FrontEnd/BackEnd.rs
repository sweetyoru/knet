use std::mem;
use std::io::{self, Read, Write};
use std::net::{IpAddr, TcpStream};

use KotoDerive::KotoTrait;
use Koto::{KotoTrait, KotoError};
use EasyCrypto::BlockCipher::{BlockCipher, BlockCipherTrait, BlockMode, PaddingMode, Aes256Cipher, Aes256CipherKey};

use crate::Misc;
use crate::DataStore::{NodeID, Node, ObjectID, Object};


#[derive(Debug, Clone)]
pub struct User
{
	username: String,
	password: String,
}

impl User
{
	pub fn New (username: &str, password: &str) -> Self
	{
		Self
		{
			username: username.to_string (),
			password: password.to_string (),
		}
	}

	pub fn Username (&self) -> &str { &self.username }
	pub fn Password (&self) -> &str { &self.password }
}


#[derive(Debug, Clone, PartialEq, Eq, KotoTrait)]
pub struct DataStoreID([u8; 64]);

impl DataStoreID
{
	pub const fn New (id: [u8; 64]) -> Self { Self(id) }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, KotoTrait)]
enum RequestType
{
	CreateNode = 1,
	GetNode = 2,
	UpdateNode = 3,
	DeleteNode = 4,
	CreateObject = 5,
	GetObject = 6,
	DeleteObject = 7,
}

impl RequestType
{
	pub fn New (backEndStatusCode: usize) -> Self
	{
		match backEndStatusCode
		{
			x if x == Self::CreateNode as usize => Self::CreateNode,
			x if x == Self::GetNode as usize => Self::GetNode,
			x if x == Self::UpdateNode as usize => Self::UpdateNode,
			x if x == Self::DeleteNode as usize => Self::DeleteNode,
			x if x == Self::CreateObject as usize => Self::CreateObject,
			x if x == Self::GetObject as usize => Self::GetObject,
			x if x == Self::DeleteObject as usize => Self::DeleteObject,
			x => panic! ("Unknown request type: {}", x),
		}
	}
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, KotoTrait)]
enum RespondStatusCode
{
	NoError = 0,

	MissingUsername = 100,
	MissingPassword = 101,

	WrongUsername = 102,
	WrongPassword = 103,

	UserNotExist = 104,

	MissingDatastoreID = 105,
	DatastoreNotExist = 106,
	NoPermission = 107,

	NodeNotExist = 108,
	ObjectNotExist = 109,

	NodeExisted = 110,
	ObjectExisted = 111,

	MissingNodeID = 112,
	MissingObjectID = 113,

	UnknownError = 200,
}

impl RespondStatusCode
{
	pub fn New (backEndStatusCode: usize) -> Self
	{
		match backEndStatusCode
		{
			x if x == Self::NoError as usize => Self::NoError,
			x if x == Self::MissingUsername as usize => Self::MissingUsername,
			x if x == Self::MissingPassword as usize => Self::MissingPassword,
			x if x == Self::WrongUsername as usize => Self::WrongUsername,
			x if x == Self::WrongPassword as usize => Self::WrongPassword,
			x if x == Self::UserNotExist as usize => Self::UserNotExist,
			x if x == Self::MissingDatastoreID as usize => Self::MissingDatastoreID,
			x if x == Self::DatastoreNotExist as usize => Self::DatastoreNotExist,
			x if x == Self::NoPermission as usize => Self::NoPermission,
			x if x == Self::NodeNotExist as usize => Self::NodeNotExist,
			x if x == Self::ObjectNotExist as usize => Self::ObjectNotExist,
			x if x == Self::NodeExisted as usize => Self::NodeExisted,
			x if x == Self::ObjectExisted as usize => Self::ObjectExisted,
			x if x == Self::MissingNodeID as usize => Self::MissingNodeID,
			x if x == Self::MissingObjectID as usize => Self::MissingObjectID,
			x if x == Self::UnknownError as usize => Self::UnknownError,
			x => panic! ("Unknown backend status code: {}", x),
		}
	}
}


#[derive(Debug, KotoTrait)]
struct CreateNodeRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	nodeID: &'a NodeID,
	isEncrypted: bool,
	nodeData: Vec<u8>,
}


#[derive(Debug, KotoTrait)]
struct CreateNodeRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,
}


#[derive(Debug, KotoTrait)]
struct GetNodeRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	nodeID: &'a NodeID,
}


#[derive(Debug, KotoTrait)]
struct GetNodeRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,

	isEncrypted: bool,
	nodeData: Vec<u8>,
}


#[derive(Debug, KotoTrait)]
struct UpdateNodeRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	nodeID: &'a NodeID,
	isEncrypted: bool,
	nodeData: Vec<u8>,
}


#[derive(Debug, KotoTrait)]
struct UpdateNodeRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,
}


#[derive(Debug, KotoTrait)]
struct DeleteNodeRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	nodeID: &'a NodeID,
}


#[derive(Debug, KotoTrait)]
struct DeleteNodeRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,
}


#[derive(Debug, KotoTrait)]
struct CreateObjectRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	objectID: &'a ObjectID,
	isEncrypted: bool,
	objectData: Vec<u8>,
}


#[derive(Debug, KotoTrait)]
struct CreateObjectRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,
}


#[derive(Debug, KotoTrait)]
struct GetObjectRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	objectID: &'a ObjectID,
}


#[derive(Debug, KotoTrait)]
struct GetObjectRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,

	objectID: ObjectID,
	isEncrypted: bool,
	objectData: Vec<u8>,
}


#[derive(Debug, KotoTrait)]
struct DeleteObjectRequest<'a>
{
	requestID: u128,
	requestType: RequestType,

	username: &'a str,
	password: &'a str,
	dataStoreID: &'a DataStoreID,

	objectID: &'a ObjectID,
}


#[derive(Debug, KotoTrait)]
struct DeleteObjectRespond
{
	requestID: u128,
	respondStatusCode: RespondStatusCode,
}


#[derive(Debug, Clone)]
pub struct BackEndServer
{
	address: IpAddr,
	port: u16,

	user: User,
	dataStoreID: DataStoreID,
	key: Aes256CipherKey,
	iv: [u8; Aes256Cipher::BLOCK_SIZE],
	readable: bool,
	writable: bool,
}

impl BackEndServer
{
	pub fn New (address: IpAddr, port: u16, user: User, dataStoreID: DataStoreID, key: Aes256CipherKey, iv: [u8; Aes256Cipher::BLOCK_SIZE], readable: bool, writable: bool) -> Self
	{
		Self
		{
			address,
			port,
			user,
			dataStoreID,
			key,
			iv,
			readable,
			writable,
		}
	}

	pub fn User (&self) -> &User { &self.user }
	pub fn Username (&self) -> &str { &self.user.username }
	pub fn Password (&self) -> &str { &self.user.password }
	pub fn DataStoreID (&self) -> &DataStoreID { &self.dataStoreID }
	pub fn Key (&self) -> &Aes256CipherKey { &self.key }
	pub fn IV (&self) -> &[u8] { &self.iv }
	pub fn Readable (&self) -> bool { self.readable }
	pub fn Writable (&self) -> bool { self.writable }


	fn SendDataToBackEnd (stream: &mut TcpStream, buffer: &[u8]) -> Result<(), BackEndError>
	{
		// send size
		let dataSize = u64::to_be_bytes (buffer.len ().try_into ().unwrap ());
		stream.write (&dataSize)?;


		// send data
		stream.write (buffer)?;

		Ok(())
	}

	fn ReceiveDataFromBackEnd (stream: &mut TcpStream) -> Result<Vec<u8>, BackEndError>
	{
		// get size
		let mut tmp = [0u8; 8];

		let readSize = stream.read (&mut tmp)?;

		if readSize != 8
		{
			return Err(BackEndError::InvalidData);
		}

		let dataSize = u64::from_be_bytes (tmp);


		// alloc buffer
		let mut result = vec! [0u8; dataSize.try_into ().unwrap ()];


		// read buffer
		let readSize = stream.read (&mut result)?;

		if readSize != usize::try_from (dataSize).unwrap ()
		{
			return Err(BackEndError::InvalidData);
		}

		Ok(result)
	}


	pub fn CreateNode (&self, node: &Node) -> Result<(), BackEndError>
	{
		assert! (self.Writable ());


		// create the request
		let nodeData = if node.IsEncrypted ()
		{
			// encrypt this
			let aes256Cipher = Aes256Cipher::New (self.Key ());
			let blockCipher = BlockCipher::New (aes256Cipher, BlockMode::CBC, PaddingMode::PKCS, self.IV ());
			blockCipher.Encrypt (&node.ToBytes ()?)
		}
		else
		{
			node.ToBytes ()?
		};

		let createNodeRequest = CreateNodeRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::CreateNode,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			nodeID: node.NodeID (),
			isEncrypted: node.IsEncrypted (),
			nodeData,
		};

		let request = createNodeRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (createNodeRespond, _) = CreateNodeRespond::FromBytes (&result)?;

		match createNodeRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::NodeNotExist => Err(BackEndError::UnknownError),
			RespondStatusCode::NodeExisted => Err(BackEndError::NodeExisted(node.NodeID ().clone ())),
			RespondStatusCode::MissingNodeID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}
	}

	pub fn GetNode (&self, nodeID: &NodeID) -> Result<Node, BackEndError>
	{
		assert! (self.Readable ());


		// create the request
		let getNodeRequest = GetNodeRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::GetNode,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			nodeID: nodeID,
		};

		let request = getNodeRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (getNodeRespond, _) = GetNodeRespond::FromBytes (&result)?;

		match getNodeRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::NodeNotExist => Err(BackEndError::NodeNotExist(nodeID.clone ())),
			RespondStatusCode::NodeExisted => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingNodeID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}?;

		let nodeData = if getNodeRespond.isEncrypted
		{
			// decrypt this
			let aes256Cipher = Aes256Cipher::New (self.Key ());
			let blockCipher = BlockCipher::New (aes256Cipher, BlockMode::CBC, PaddingMode::PKCS, self.IV ());
			blockCipher.Decrypt (&getNodeRespond.nodeData, true)
		}
		else
		{
			getNodeRespond.nodeData
		};

		let (node, _) = Node::FromBytes (&nodeData)?;

		Ok(node)
	}

	pub fn UpdateNode (&self, node: &Node) -> Result<(), BackEndError>
	{
		assert! (self.Writable ());


		// create the request
		let nodeData = if node.IsEncrypted ()
		{
			// encrypt this
			let aes256Cipher = Aes256Cipher::New (self.Key ());
			let blockCipher = BlockCipher::New (aes256Cipher, BlockMode::CBC, PaddingMode::PKCS, self.IV ());
			blockCipher.Encrypt (&node.ToBytes ()?)
		}
		else
		{
			node.ToBytes ()?
		};

		let updateNodeRequest = UpdateNodeRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::UpdateNode,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			nodeID: node.NodeID (),
			isEncrypted: node.IsEncrypted (),
			nodeData,
		};

		let request = updateNodeRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (updateNodeRespond, _) = UpdateNodeRespond::FromBytes (&result)?;

		match updateNodeRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::NodeNotExist => Err(BackEndError::NodeNotExist(node.NodeID ().clone ())),
			RespondStatusCode::NodeExisted => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingNodeID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}
	}

	pub fn DeleteNode (&self, nodeID: &NodeID) -> Result<(), BackEndError>
	{
		assert! (self.Writable ());


		// create the request
		let deleteNodeRequest = DeleteNodeRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::DeleteNode,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			nodeID,
		};

		let request = deleteNodeRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (deleteNodeRespond, _) = DeleteNodeRespond::FromBytes (&result)?;

		match deleteNodeRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::NodeNotExist => Err(BackEndError::NodeNotExist(nodeID.clone ())),
			RespondStatusCode::NodeExisted => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingNodeID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}
	}


	pub fn CreateObject (&self, object: &Object) -> Result<(), BackEndError>
	{
		assert! (self.Writable ());


		// create the request
		let objectData = if object.IsEncrypted ()
		{
			// encrypt this
			let aes256Cipher = Aes256Cipher::New (self.Key ());
			let blockCipher = BlockCipher::New (aes256Cipher, BlockMode::CBC, PaddingMode::PKCS, self.IV ());
			blockCipher.Encrypt (&object.ToBytes ()?)
		}
		else
		{
			object.ToBytes ()?
		};

		let createObjectRequest = CreateObjectRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::CreateObject,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			objectID: object.ObjectID (),
			isEncrypted: object.IsEncrypted (),
			objectData,
		};

		let request = createObjectRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (createObjectRespond, _) = CreateObjectRespond::FromBytes (&result)?;

		match createObjectRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::ObjectNotExist => Err(BackEndError::UnknownError),
			RespondStatusCode::ObjectExisted => Err(BackEndError::ObjectExisted(object.ObjectID ().clone ())),
			RespondStatusCode::MissingObjectID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}
	}

	pub fn GetObject (&self, objectID: &ObjectID) -> Result<Object, BackEndError>
	{
		assert! (self.Readable ());


		// create the request
		let getObjectRequest = GetObjectRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::GetObject,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			objectID,
		};

		let request = getObjectRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (getObjectRespond, _) = GetObjectRespond::FromBytes (&result)?;

		match getObjectRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::ObjectNotExist => Err(BackEndError::ObjectNotExist(objectID.clone ())),
			RespondStatusCode::ObjectExisted => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingObjectID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}?;

		let objectData = if getObjectRespond.isEncrypted
		{
			// decrypt this
			let aes256Cipher = Aes256Cipher::New (self.Key ());
			let blockCipher = BlockCipher::New (aes256Cipher, BlockMode::CBC, PaddingMode::PKCS, self.IV ());
			blockCipher.Decrypt (&getObjectRespond.objectData, true)
		}
		else
		{
			getObjectRespond.objectData
		};

		let (object, _) = Object::FromBytes (&objectData)?;

		Ok(object)
	}

	pub fn DeleteObject (&self, objectID: &ObjectID) -> Result<(), BackEndError>
	{
		assert! (self.Writable ());


		// create the request
		let deleteObjectRequest = DeleteObjectRequest
		{
			requestID: Misc::GetRandomU128 (),
			requestType: RequestType::DeleteObject,

			username: self.Username (),
			password: self.Password (),
			dataStoreID: self.DataStoreID (),

			objectID,
		};

		let request = deleteObjectRequest.ToBytes ()?;


		// connect
		let mut stream = TcpStream::connect ((self.address, self.port))?;


		// send the request
		Self::SendDataToBackEnd (&mut stream, &request)?;


		// get the respond
		let result = Self::ReceiveDataFromBackEnd (&mut stream)?;
		let (getObjectRespond, _) = GetObjectRespond::FromBytes (&result)?;

		match getObjectRespond.respondStatusCode
		{
			RespondStatusCode::NoError => Ok(()),

			RespondStatusCode::MissingUsername => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingPassword => Err(BackEndError::UnknownError),

			RespondStatusCode::WrongUsername => Err(BackEndError::WrongUsername(self.User ().clone ())),
			RespondStatusCode::WrongPassword => Err(BackEndError::WrongPassword(self.User ().clone ())),

			RespondStatusCode::UserNotExist => Err(BackEndError::UserNotExist(self.User ().clone ())),

			RespondStatusCode::MissingDatastoreID => Err(BackEndError::UnknownError),
			RespondStatusCode::DatastoreNotExist => Err(BackEndError::DataStoreNotExist(self.DataStoreID ().clone ())),
			RespondStatusCode::NoPermission => Err(BackEndError::NoPermission(self.User ().clone (), self.DataStoreID ().clone ())),

			RespondStatusCode::ObjectNotExist => Err(BackEndError::ObjectExisted(objectID.clone ())),
			RespondStatusCode::ObjectExisted => Err(BackEndError::UnknownError),
			RespondStatusCode::MissingObjectID => Err(BackEndError::UnknownError),
			RespondStatusCode::UnknownError => Err(BackEndError::UnknownError),
			_ => Err(BackEndError::UnknownError),
		}
	}
}


#[derive(Debug)]
pub enum BackEndError
{
	UserNotExist(User),
	WrongUsername(User),
	WrongPassword(User),
	NoPermission(User, DataStoreID),
	DataStoreNotExist(DataStoreID),
	NodeNotExist(NodeID),
	NodeExisted(NodeID),
	ObjectNotExist(ObjectID),
	ObjectExisted(ObjectID),
	UnknownError,
	IOError(io::Error),
	KotoError(KotoError),
	InvalidData,
}


impl From<io::Error> for BackEndError
{
	fn from (error: io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<KotoError> for BackEndError
{
	fn from (error: KotoError) -> Self
	{
		Self::KotoError(error)
	}
}
