use std::{fmt, fs};
use std::sync::RwLock;
use std::time::Duration;

use serde_json::Value;
use serde::Deserialize;


#[derive (Debug, Clone, Deserialize)]
pub struct AppConfig
{
	guiConfigPath: String,
	dataSavePath: String,

	backEndConnectTimeout: Duration,
}

impl AppConfig
{
	pub fn GuiConfigPath (&self) -> &str { &self.guiConfigPath }
	pub fn DataSavePath (&self) -> &str { &self.dataSavePath }

	pub fn BackEndConnectTimeout (&self) -> Duration { self.backEndConnectTimeout }
}


static mut globalAppConfig: Option<RwLock<AppConfig>> = None;


pub fn InitGlobalConfig (configPath: &str) -> Result<(), ConfigError>
{
	let configFileContent = fs::read_to_string (configPath)?;
	let configData: Value = serde_json::from_str (&configFileContent)?;

	unsafe
	{
		globalAppConfig = Some(RwLock::new (AppConfig
		{
			guiConfigPath: String::deserialize (&configData["guiConfigPath"])?,
			dataSavePath: String::deserialize (&configData["dataSavePath"])?,

			backEndConnectTimeout: Duration::deserialize (&configData["backEndConnectTimeout"])?,
		}));
	}

	Ok(())
}


pub fn GetGlobalAppConfig () -> Result<&'static RwLock<AppConfig>, ConfigError>
{
	unsafe
	{
		match &globalAppConfig
		{
			Some(appConfig) => Ok(&appConfig),
			None => Err(ConfigError::UninitializedConfig),
		}
	}
}


#[derive (Debug)]
pub enum ConfigError
{
	IOError(std::io::Error),
	JsonError(serde_json::Error),
	UninitializedConfig,
}

impl std::error::Error for ConfigError {}

impl fmt::Display for ConfigError
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let result = match self
		{
			Self::IOError(error) => format! ("IO error: {}", error),
			Self::JsonError(error) => format! ("Json error: {}", error),
			Self::UninitializedConfig => String::from ("Uninitialized config"),
		};

		write! (f, "{}", result)
	}
}

impl From<std::io::Error> for ConfigError
{
	fn from (error: std::io::Error) -> Self
	{
		Self::IOError(error)
	}
}

impl From<serde_json::Error> for ConfigError
{
	fn from (error: serde_json::Error) -> Self
	{
		Self::JsonError(error)
	}
}
